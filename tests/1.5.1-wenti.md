### 问题

之所以要规划做笔记的方法，是因为现在实际上我的大部分时间在做无效记录。

所谓无效记录就是：笔记数量一致增加，却对生活没有帮助。老是抱着“随手记录下来以后也许用得着“的心态，往往以后用得着的机会太少了，却花了大量的时间用于记录。
