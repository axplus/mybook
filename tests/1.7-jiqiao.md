## 技巧

* 笔记本旁放一支笔，让想法可以随时记录

* 画草图来帮助思考。如果不知道从哪里入手，那么最好的方式就是把所有“关键词”写在一张白纸上，连接词与词之间的关系，就可以把系统理清楚来


