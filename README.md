# 如何记笔记
_2018-05-11_



[TOC]

## 看看别人如何做笔记

每个人都有自己的笔记方法，对自己是有效的。

不能总结出“最”有效的方法。

我看过很多人如何记笔记，于是就混乱了，似乎照搬每种方法刚开始都挺好的，但是渐渐就显露出弊端了。


## 笔记种类

iPad Pro

Surface Pro

iPad Mini


## 轻量级笔记方案

## 笔记的分类

1. 只能用来搜索，本身并没有什么内容，就是用来记录
2. 某个事情的一部分信息
  1. 不能整理
  2. 可以整理



## 减少“不必要”的笔记
### 问题

之所以要规划做笔记的方法，是因为现在实际上我的大部分时间在做无效记录。

所谓无效记录就是：笔记数量一致增加，却对生活没有帮助。老是抱着“随手记录下来以后也许用得着“的心态，往往以后用得着的机会太少了，却花了大量的时间用于记录。

### 现况

Evernote中存在大量的笔记，这些笔记有些只是记录性的，本身不提供任何整理价值。

还有一些是有整理价值的，但是应该是不断更新的，所以保留当时的记录是没有必要的。

所以现在Evernote是一个被我用来记录流水账的地方。


### 一个方案：分离笔记


#### 基本

分两个笔记本：
1. 笔记本1：随便记、草稿、记事、记录任何临时性的东西，按时间记录，记录到同一个地方；
2. 笔记本2：从笔记本1整理而来，需要认真记，记录持久性的事物。

勤于整理。**只有经过整理的东西，经过了大脑，才能在大脑中留下印象。**





一定需要有一本可以随时记录的笔记本，记录下各种想法、画图，这是因为：
1. 一定不需要做记录之前分类，这样会纠结“放在哪个笔记本”等问题
2. 一定不要认真记录，这样会纠结“用什么字体”等问题





#### 如何整理

笔记本2只记录**整理过**的笔记。笔记本1中的信息被整理的时候就要删除掉，减少负担。

笔记本1应该可以不停的翻页跳转的，只有这样才能方便地把思维归纳到一起。


## 如何读书

1. 在书上做笔记
2. 使用书签记录阅读的进度
3. 整理读书笔记到笔记本2，只有经过整理才能让书的内容经过大脑，留下印象





## 技巧

* 笔记本旁放一支笔，让想法可以随时记录

* 画草图来帮助思考。如果不知道从哪里入手，那么最好的方式就是把所有“关键词”写在一张白纸上，连接词与词之间的关系，就可以把系统理清楚来



## 为了写书，我开发了个软件

### gitbook, ulysses, ...

我选择自己写。
  ​
### History

0.1.0

support split/merge

0.2.0


support multi level split

0.2.1

change parse method

0.3.0

multi level numbering
