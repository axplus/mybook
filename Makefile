test_chapters:=$(wildcard tests/*.md)

README.md: $(test_chapters)
	python -mmybook merge -o$@ $(dir $<)

split: README.md
	python -mmybook split -Otests $<

