# 2018-04-11


import argparse
import re

import pypinyin
from logging import info

hadpat = re.compile(r'(#+)\s+(.+)')


def guessname(firstline):
    m = hadpat.match(firstline.strip())
    n = m.group(2)
    arr = pypinyin.lazy_pinyin(n.decode('utf-8'), style=pypinyin.Style.NORMAL)
    # info(arr)
    n1 = ''.join(arr)
    n2 = n1.replace(' ', '-')
    return n2


def getheader(linestr):
    m = hadpat.match(linestr)
    tag = m.group(1)
    return len(tag), m.group(2)


def iter_chapter(all_lines):
    # init
    # normal
    # newchapter
    # term
    mode = 'init'
    buffer = []
    i = 0
    while mode != 'term':  # 1:##i < len(all_lines):
        # info(i,line)
        if mode == 'init':
            mode = 'normal'
        elif mode == 'newchapter':
            if buffer:
                depth, _ = getheader(buffer[0])
                yield depth, buffer
            buffer = []  # clean
            mode = 'normal'
        elif mode == 'normal':
            if i < len(all_lines):
                buffer.append(all_lines[i])  # push current line
                i += 1  # peek next line
                if i+1 < len(all_lines):
                    next_line = all_lines[i]
                    try:
                        level, _ = getheader(next_line)
                        # if level <= slevel:
                        # if line.startswith('## '):
                        mode = 'newchapter'
                        continue
                    except:
                        pass
            else:
                if buffer:
                    mode = 'newchapter'
                else:
                    mode = 'term'
        elif mode == 'term':
            pass

    # if buffer:
    #     yield buffer


def split(inpfilename, output_directory, slevel=3):
    all_lines = open(inpfilename).read().split('\n')
    info(len(all_lines))
    depthorders = {}
    lastdepth = 0
    for i, (depth, chapter) in enumerate(iter_chapter(all_lines)):
        if depth > lastdepth:
            order = 1
        else:
            order = depthorders.get(depth, 1)+1
        lastdepth = depth
        depthorders[depth] = order

        lst = [str(depthorders.get(i+1, 1))for i in range(depth)]
        od = '.'.join(lst)
        # info('depth=%s lst=%s chapter=%s',depth, '.'.join(lst), chapter[:1])
        name = guessname(chapter[0])
        fn = '%s/%s-%s.md' % (output_directory, od, name)
        open(fn, 'w').write('\n'.join(chapter))
