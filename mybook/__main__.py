# 2018-4-11


import argparse
import sys
from . import merge
from . import split

parser = argparse.ArgumentParser()

cmd = sys.argv[1]
if cmd == 'split':
    parser.add_argument('-O', '--output_directory', default='.')
    parser.add_argument('inp')
    args = parser.parse_args(sys.argv[2:])
    split.split(args.inp, args.output_directory)


elif cmd == 'merge':
    parser.add_argument('-o', '--output')
    parser.add_argument('dire')
    args = parser.parse_args(sys.argv[2:])
    chapter_filenames = merge.find_split_chapter_filenames(args.dire)
    merge.merge(chapter_filenames, args.output)
