# 2018-04-11


import os
import re
from logging import info

filepat = re.compile(r'([\d\.]+)[\-\s]*.+\.md')


def getorder(fnstr):
    n = os.path.basename(fnstr)
    m = filepat.match(n)
    ver = m.group(1)
    vers = ver.split('.')
    n = sum([int(v)*(100**(10-i)) for i, v in enumerate(vers)])
    return n


def find_split_chapter_filenames(dire):
    filenames = []
    for r, ds, fs in os.walk(dire):  # scandir(dire):
        for f in fs:
            p = os.path.join(r, f)
            if not os.path.isfile(p):
                continue

            m = filepat.match(f)
            if not m:
                continue

            filenames.append(p)
    filenames.sort(key=lambda fn: getorder(fn))
    info(filenames)
    return filenames


def merge(filenames, outfilename):
    # filenames = find_split_chapter_filenames(basedir)
    buff = []
    for f in filenames:
        c = open(f).read()
        buff.append(c)
    open(outfilename, 'w').write('\n'.join(buff))
